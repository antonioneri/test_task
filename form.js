
// Tutti i campi, tranne l’oggetto, devono essere obbligatori, se non compilati deve essere mostrato un messaggio di errore

function validate() {

    let name = document.getElementById("name");
    let surname = document.getElementById("surname");
    let email = document.getElementById("email");
    let mess = document.getElementById("mess");
    let errorBox = document.getElementById("errorMessage");
    let alertDiv = '<div class="alert alert-danger alert-dismissible" role="alert">';
    let alertbtn = '<a type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></a>';
    let checkName = /^[a-zA-Z]+$/;
    // let checkEmail =  /^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;


    name.style.borderBottom = "2px solid #999";
    surname.style.borderBottom = "2px solid #999";
    email.style.borderBottom = "2px solid #999";
    mess.style.borderBottom = "2px solid #999";
    

            //regole per il nome
    if (name.value == "") {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci nome' + "</div>"
        name.focus();
        name.style.borderBottom = "2px solid #990033";
        return false;
    }
     if(!checkName.test(name.value)) {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci nome corretto' + "</div>"
        name.focus();
        name.style.borderBottom = "2px solid #990033";
        return false;
    }
            // regole per il cognome
    if (surname.value == "") {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci cognome' + "</div>"
        surname.focus();
        surname.style.borderBottom = "2px solid #990033";
        return false;
    }
    if(!checkName.test(surname.value)) {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci cognome corretto' + "</div>"
        surname.focus();
        surname.style.borderBottom = "2px solid #990033";
        return false;
    }

    if (email.value == "") {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci email' + "</div>"
        email.focus();
        email.style.borderBottom = "2px solid #990033";
        return false;
    }

    // if(!checkEmail.test(email.value)) {
    //     errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci email corretta' + "</div>"
    //     email.focus();
    //     email.style.borderBottom = "2px solid #990033";
    //     return false;
    // }

    if (mess.value == "") {
        errorBox.innerHTML = alertDiv + alertbtn + 'Inserisci del testo' + "</div>"
        mess.focus();
        mess.style.borderBottom = "2px solid #990033";
        return false;
    }
 
    return true;

}


// Il tasto invia deve essere disabilitato finché non viene accettata la privacy

const submitButton = document.getElementById('submitButton');
submitButton.disabled = true;

const privacyCheckbox = document.getElementById('privacyCheckbox');
privacyCheckbox.addEventListener('change', function () {
    submitButton.disabled = !privacyCheckbox.checked;
});


// invio dati AJAX

        // Invia i dati al server
                
                $("form").submit(function(e) {
                    e.preventDefault();
                    if(!validate()) return;
                    let formData = {
                        name: $("#name").val(),
                        email: $("#email").val(),
                        surname: $('#surname').val(),
                        object: $('#object').val(),
                        mess: $('#mess').val()
                    };
                    
                    $.ajax({
                        type: "POST",
                        url: "process.php",
                        data: $("form").serialize(),
                    }).done( function (data){ 
                        console.log(data);
                            let successBox = document.getElementById("successMessage");
                        successBox.innerHTML = '<div class="alert alert-success" role="alert"> Dati inviati con successo</div>';
                  });
                  
                });


            



            
   